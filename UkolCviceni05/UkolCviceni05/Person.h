#ifndef PERSON_H
#define PERSON_H
#include<string>
namespace Entity {
	class Person {
	public:
		Person(int aId, std::string aName, std::string aTelNumber);
		~Person();
		void setId(int aId);
		int getId() const;
		void setName(std::string aName);
		std::string getName() const;
		void setTelNumber(std::string aTelNumber);
		std::string getTelNumber() const;
	private:
		int id;
		std::string name;
		std::string telNumber;
	};
}
#endif // !PERSON_H

