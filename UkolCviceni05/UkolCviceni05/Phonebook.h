#ifndef PHONEBOOK_H
#define PHONEBOOK_H
#include "Person.h"
#include <stdexcept>

class ElementDoesNotExist :public std::exception {
public:
	ElementDoesNotExist(const std::string& aMess);
	virtual const char* what() const noexcept override;
private:
	std::string mess;
};

namespace Model {
	class LinkListElement {
	public:
		LinkListElement(LinkListElement* aNext, Entity::Person aData);
		~LinkListElement();
		LinkListElement* next;
		Entity::Person data;
	};

	class Phonebook {
	public:
		Phonebook();
		~Phonebook();
		void addPerson(Entity::Person aPerson);
		std::string findTelNumber(int id) const;
		std::string findTelNumber(std::string name) const;
	private:
		LinkListElement* first;
		int size;
	};
}

#endif // !PHONEBOOK_H
