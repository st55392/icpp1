#include "Phonebook.h"
#include "Person.h"
#include <string>
#include <iostream>
using namespace std;
int main(int argc, char** argv) {
	Model::Phonebook* phoneBook = new Model::Phonebook();
	for (int i = 0; i < 50; i++) {
		string name = "Name"+to_string(i);
		string telNumber = "222 222 22" + to_string(i);
		Entity::Person* person = new Entity::Person(i, name, telNumber);
		phoneBook->addPerson(*person);
	}

	try {
		cout << phoneBook->findTelNumber("Name1") << endl;
		//cout << phoneBook->findTelNumber("")<<endl;
		cout << phoneBook->findTelNumber("JMENO")<<endl;
	}
	catch (ElementDoesNotExist& ex) {
		cout << ex.what() << endl;
	}
	catch (invalid_argument& ex) {
		cout << ex.what() << endl;
	}

	try{
		cout << phoneBook->findTelNumber(1)<<endl;
		//cout << phoneBook->findTelNumber(-2)<<endl;
		cout << phoneBook->findTelNumber(1000)<<endl;
	}
	catch (ElementDoesNotExist& ex) {
		cout << ex.what() << endl;
	}
	catch (invalid_argument& ex) {
		cout << ex.what() << endl;
	}

	return 0;
}
