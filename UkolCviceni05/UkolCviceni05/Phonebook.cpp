#include "Phonebook.h"

Model::Phonebook::Phonebook()
{
	first = nullptr;
	size = 0;
}

Model::Phonebook::~Phonebook()
{
	while (first != nullptr) {
		LinkListElement* element = first->next;
		delete first;
		first = element;
	}
}

void Model::Phonebook::addPerson(Entity::Person aPerson)
{
	if(size>0){
		LinkListElement* temp = first;
		while (temp->next != nullptr) {
			temp = temp->next;
		}
		temp->next = new LinkListElement(nullptr, aPerson);
		size++;
	}
	else {
		first = new LinkListElement(nullptr, aPerson);
		size++;
	}
}

std::string Model::Phonebook::findTelNumber(int id) const
{
	if (id < 0) {
		throw std::invalid_argument("Id must be bigger then or equal to zero!");
	}
	if (size > 0) {
		LinkListElement* temp = first;
		while (temp->next != nullptr) {
			if (temp->data.getId() == id) {
				return temp->data.getTelNumber();
			}
			temp = temp->next;
		}
	}
	throw ElementDoesNotExist("Finding tel. number does not exist");
}

std::string Model::Phonebook::findTelNumber(std::string name) const
{
	if (name.length()==0) {
		throw std::invalid_argument("String in parameter is not correct!");
	}
	if (size > 0) {
		LinkListElement* temp = first;
		while (temp->next != nullptr) {
			if (temp->data.getName().compare(name) == 0) {
				return temp->data.getTelNumber();
			}
			temp = temp->next;
		}
	}
	throw ElementDoesNotExist("Finding tel. number does not exist");
}

Model::LinkListElement::LinkListElement(LinkListElement* aNext, Entity::Person aData): next(aNext),data(aData)
{
}

Model::LinkListElement::~LinkListElement()
{
}

ElementDoesNotExist::ElementDoesNotExist(const std::string& aMess) :mess(aMess)
{
}

const char* ElementDoesNotExist::what() const noexcept
{
	return mess.c_str();
}
