#include "Person.h"

Entity::Person::Person(int aId, std::string aName, std::string aTelNumber):id(aId),name(aName),telNumber(aTelNumber)
{
}

Entity::Person::~Person()
{
}

void Entity::Person::setId(int aId)
{
	id = aId;
}

int Entity::Person::getId() const
{
	return id;
}

void Entity::Person::setName(std::string aName)
{
	name = aName;
}

std::string Entity::Person::getName() const
{
	return name;
}

void Entity::Person::setTelNumber(std::string aTelNumber)
{
	telNumber = aTelNumber;
}

std::string Entity::Person::getTelNumber() const
{
	return telNumber;
}
