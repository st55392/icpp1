#include "Pokladna.h"
#include<iostream>
using namespace std;
int main(int argc, char** argv)
{
	/*Ve funkci main() t��dy otestujte � vytvo�te instanci t��dy Pokladna a n�sledn� vystavte
	alespo� 3 r�zn� ��tenky.*/
	Pokladna* pok = new Pokladna();
	Uctenka uct1 = pok->vystavUctenku(123.4, 21);
	Uctenka uct2 = pok->vystavUctenku(12.2, 15);
	Uctenka uct3 = pok->vystavUctenku(223.9, 15);

	/*Vyzkou�ejte manipulaci s ��tenkou vr�cenou metodou vystavUctenku()*/
	cout << "Uctenka c.: " << uct1.getCisloUctenky() << ", castka: " << uct1.getCastka() << ", sazba DPH: " << uct1.getDph() << " %." << endl;
	cout << "Uctenka c.: " << uct2.getCisloUctenky() << ", castka: " << uct2.getCastka() << ", sazba DPH: " << uct2.getDph() << " %."<<endl;
	cout << "Uctenka c.: " << uct3.getCisloUctenky() << ", castka: " << uct3.getCastka() << ", sazba DPH: " << uct3.getDph() << " %." << endl;

	
	Uctenka uct4 = pok->dejUctenku(1001);
	Uctenka uct5 = pok->dejUctenku(1002);
	Uctenka uct6 = pok->dejUctenku(1003);

	cout << endl;
	cout << "Uctenka c.: " << uct4.getCisloUctenky() << ", castka: " << uct4.getCastka() << ", sazba DPH: " << uct4.getDph() << " %." << endl;
	cout << "Uctenka c.: " << uct5.getCisloUctenky() << ", castka: " << uct5.getCastka() << ", sazba DPH: " << uct5.getDph() << " %." << endl;
	cout << "Uctenka c.: " << uct6.getCisloUctenky() << ", castka: " << uct6.getCastka() << ", sazba DPH: " << uct6.getDph() << " %." << endl;

	cout << "Celkova castka (bez DPH): " << pok->dejCastku() << endl;
	cout << "Celkova castka (s DPH): " << pok->dejCastkuVcDph() << endl;


	pok->~Pokladna();



	return 0;
}