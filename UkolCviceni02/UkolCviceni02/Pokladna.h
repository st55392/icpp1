#ifndef POKLADNA_H
#define POKLADNA_H
#include "Uctenka.h"

class Pokladna {
public:
	Pokladna();
	~Pokladna();
	Uctenka& vystavUctenku(double aCastka, double aDph);
	Uctenka& dejUctenku(int aCisloUctenky);
	double dejCastku() const;
	double dejCastkuVcDph() const;
private:
	Uctenka* poleUctenek;
	int pocetVydanychUctenek;
	static int citacId;
};
#endif // POKLADNA_H

