#include "Uctenka.h"

int Uctenka::getCisloUctenky() {
	return this->cisloUctenky;
}

double Uctenka::getCastka() {
	return this->castka;
}

double Uctenka::getDph() {
	return this->dph;
}

void Uctenka::setCisloUctenky(int aCisloUctenky) {
	this->cisloUctenky = aCisloUctenky;
}

void Uctenka::setCastka(const double aCastka) {
	this->castka = aCastka;
}

void Uctenka::setDph(const double aDph) {
	this->dph = aDph;
}