#include "Pokladna.h"
#define POCET_UCTENEK 10
#define VYCHOZI_HODNOTA_CITAC 1000
#include <stdexcept>

int Pokladna::citacId = VYCHOZI_HODNOTA_CITAC;

Pokladna::~Pokladna() {
	delete[] poleUctenek;
}

Pokladna::Pokladna() : pocetVydanychUctenek(0) {
	poleUctenek = new Uctenka[POCET_UCTENEK];

}

Uctenka& Pokladna::vystavUctenku(double aCastka, double aDph) {
	if (pocetVydanychUctenek == POCET_UCTENEK) {
		throw std::overflow_error("Pokladna je pln�.");
	}
	poleUctenek[pocetVydanychUctenek].setCastka(aCastka);
	poleUctenek[pocetVydanychUctenek].setDph(aDph);
	poleUctenek[pocetVydanychUctenek].setCisloUctenky(citacId++);
	pocetVydanychUctenek++;
	return poleUctenek[pocetVydanychUctenek - 1];
}

Uctenka& Pokladna::dejUctenku(int aCislo) {
	for (size_t idx = 0; idx < pocetVydanychUctenek; idx++) {
		if (poleUctenek[idx].getCisloUctenky() == aCislo) {
			return poleUctenek[idx];
		}
	}
	throw std::invalid_argument("Uctenka neexistuje.");
	//return poleUctenek[0];	
}

double Pokladna::dejCastku() const {
	double celkCastka = 0;
	for (int i = 0; i < pocetVydanychUctenek; i++) {
		celkCastka += poleUctenek[i].getCastka();
	}
	return celkCastka;
}

double Pokladna::dejCastkuVcDph() const{
	double celkCastka = 0;
	for (int i = 0; i < pocetVydanychUctenek; i++) {
		celkCastka += poleUctenek[i].getCastka()*(1+poleUctenek[i].getDph()/100);
	}
	return celkCastka;
}