#ifndef UCTENKA_H
#define UCTENKA_H

class Uctenka {
public:
	int getCisloUctenky();
	double getCastka();
	double getDph();
	void setCisloUctenky(int aCisloUctenky);
	void setCastka(const double aCastka);
	void setDph(const double aDph);
private:
	int cisloUctenky;
	double castka;
	double dph;
};

#endif //UCETENKA_H
