#ifndef PERSON_H
#define PERSON_H
#include <iostream>
#include <fstream>
#include "Address.h"
#include "Date.h"

struct Person {
public:
	Person();
	Person(std::string aName, std::string aSurname, Address aResidence, Date aBirthDay);
	~Person();
	std::string _name;
	std::string _surname;
	Address _residence;
	Date _birthDay;
};
std::ostream& operator<<(std::ostream& os, const Person& person);
std::istream& operator>>(std::istream& is, Person& person);

void saveBinary(std::ofstream& os, const Person& person);
void loadBinary(std::ifstream& is, Person& person);
#endif // !PERSON_H
