#include "Person.h"
using namespace std;
Person::Person()
{
}
Person::Person(std::string aName, std::string aSurname, Address aResidence, Date aBirthDay):_name(aName),_surname(aSurname),_residence(aResidence),_birthDay(aBirthDay)
{
}

Person::~Person()
{
}


ostream& operator<<(ostream& os, const Person& person) {
	os << person._name << "\n" << person._surname << "\n" << person._residence  << person._birthDay<<endl;
	return os;
}

istream& operator>>(istream& is, Person& person) {
	is >> person._name;
	is >> person._surname;
	is >> person._residence;
	is>> person._birthDay;
	return is;
}

void saveBinary(std::ofstream& os, const Person& person){
	int s = person._name.size() + 1;
	os.write((const char*)& s, sizeof(int));
	os.write(reinterpret_cast<char*>(&s), sizeof(int));
	os.write(person._name.c_str(), s);

	s = person._surname.size() + 1;
	os.write((const char*)& s, sizeof(int));
	os.write(reinterpret_cast<char*>(&s), sizeof(int));
	os.write(person._surname.c_str(), s);

	saveBinary(os, person._residence);
	saveBinary(os, person._birthDay);
}

void loadBinary(std::ifstream& is, Person& person){
	char* buffer;
	int s = 0;

	is.read((char*) & (s), sizeof(int));
	is.read(reinterpret_cast<char*>(&s), sizeof(int));
	buffer = new char[s];
	is.read(buffer, s);
	person._name.append(buffer, s);

	is.read((char*) & (s), sizeof(int));
	is.read(reinterpret_cast<char*>(&s), sizeof(int));
	buffer = new char[s];
	is.read(buffer, s);
	person._surname.append(buffer, s);

	loadBinary(is, person._residence);
	loadBinary(is, person._birthDay);
}
