#include "Address.h"
using namespace std;
Address::Address()
{
}
Address::Address(string aStreet, string aCity, int aZip):_zip(aZip),_street(aStreet),_city(aCity)
{
}

Address::~Address()
{
}

ostream& operator<<(ostream& os, const Address& adr) {
	os << adr._street << "\n" << adr._city << "\n" << adr._zip<<endl;
	return os;
}

istream& operator>>(istream& is, Address& adr) {
	is >> adr._street;
	is >> adr._city;
	is>> adr._zip;
	return is;
}

void saveBinary(std::ofstream& os, const Address& adr){
	int s = adr._street.size() + 1;
	os.write((const char*)& s, sizeof(int));
	os.write(reinterpret_cast<char*>(&s), sizeof(int));
	os.write(adr._street.c_str(), s);

	s = adr._city.size() + 1;
	os.write((const char*)& s, sizeof(int));
	os.write(reinterpret_cast<char*>(&s), sizeof(int));
	os.write(adr._city.c_str(), s);

	os.write((const char*)& adr._zip, sizeof(int));
}

void loadBinary(std::ifstream& is, Address& adr){
	char* buffer;
	int s = 0;

	is.read((char*) & (s), sizeof(int));
	is.read(reinterpret_cast<char*>(&s), sizeof(int));
	buffer = new char[s];
	is.read(buffer, s);
	adr._street.append(buffer, s);

	is.read((char*) & (s), sizeof(int));
	is.read(reinterpret_cast<char*>(&s), sizeof(int));
	buffer = new char[s];
	is.read(buffer, s);
	adr._city.append(buffer, s);

	is.read((char*) & (adr._zip), sizeof(int));
}


