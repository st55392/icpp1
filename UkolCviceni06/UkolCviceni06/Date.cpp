#include "Date.h"
using namespace std;
Date::Date()
{
}
Date::Date(int aDay, int aMonth, int aYear):_day(aDay),_month(aMonth),_year(aYear)
{
}

Date::~Date()
{
}


ostream& operator<<(ostream& os, const Date& date) {
	os << date._day << "\n" << date._month << "\n" << date._year<<endl;
	return os;
}

istream& operator>>(istream& is, Date& date) {
	is >> date._day;
	is >> date._month;
	is >> date._year;
	return is;
}

void saveBinary(std::ofstream& os, const Date& date){
	os.write((const char*)& date._day, sizeof(int));
	os.write((const char*)& date._month, sizeof(int));
	os.write((const char*)& date._year, sizeof(int));
}

void loadBinary(std::ifstream& is, Date& date){
	is.read((char*) & (date._day), sizeof(int));
	is.read((char*) & (date._month), sizeof(int));
	is.read((char*) & (date._year), sizeof(int));
}
