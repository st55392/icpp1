#ifndef ADDRESS_H
#define ADDRESS_H
#include <string>
#include <iostream>
#include <fstream>

struct Address {
	Address();
	Address(std::string aStreet, std::string aCity, int aZip);
	~Address();
	std::string _street;
	std::string _city;
	int _zip;
};
std::ostream& operator<<(std::ostream& os, const Address& adr);
std::istream& operator>>(std::istream& is, Address& adr);

void saveBinary(std::ofstream& os, const Address& adr);
void loadBinary(std::ifstream& is, Address& adr);
#endif // !ADDRESS_H