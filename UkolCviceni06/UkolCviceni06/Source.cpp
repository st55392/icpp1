#include "Person.h"
#include <fstream>
#include <iostream>

#define NUMBER_OF_PERSONS 100
using namespace std;

void nacti() {
	int max = 0;
	int nacteno = 0;
	ifstream in{};
	string slovo{};
	in.open("pokus.txt");
	if (in.is_open()) {
		in >> max;
		Person* persons = new Person[max];

		for (int i = 0; i < max; i++) {
			in >> persons[i];
			nacteno++;
		}
		cout << "Nacteno bylo: " << nacteno << " osob" << endl;
	}
	else {
		cerr << "Nepodarilo se nacist";
	}
}

void uloz() {
	Person person = Person("Jmeno1", "Prijmeni1", Address("Ulice", "Mesto", 55532), Date(1, 2, 2000));
	int ulozeno = 0;
	ofstream out{};
	out.open("pokus.txt");
	if (out.is_open()) {
		out << NUMBER_OF_PERSONS;
		for (int i = 0; i < NUMBER_OF_PERSONS; i++) {
			out << person;
			ulozeno++;
		}
		out.close();
		cout << "Ulozeno bylo: " << ulozeno << " osob"<<endl;
	}
	else {
		cerr << "Nepodarilo se zapsat"<<endl;
	}
}

void saveBinary() { //funkce ve stavu pokusu :D
	ofstream out{};
	out.open("binary.dat",ios_base::binary);
	Person person = Person("Jmeno1", "Prijmeni1", Address("Ulice", "Mesto", 55532), Date(1, 2, 2000));
	int personsNumber = NUMBER_OF_PERSONS;
	if (out.is_open()) {
		out.write((const char*)&personsNumber, sizeof(int));
		for (int i = 0; i < NUMBER_OF_PERSONS; i++) {
			saveBinary(out, person);
		}
	}
}

void loadBinary() {
	ifstream in{};
	in.open("binary.dat", ios_base::binary);
	if (in.is_open()) {
		int s = 0;
		string slovo{};
		int n = 0;

		in.read((char*)& s, sizeof(int));
		Person* persons = new Person[s];

		for (int i = 0; i < s; i++) {
			loadBinary(in, persons[i]);
			cout << persons[i] << endl;
		}
		delete[]persons;
	}
	

	
}

int main(int argc, char** argv) {
	//uloz();
	//nacti();
	saveBinary();
	loadBinary();
}



