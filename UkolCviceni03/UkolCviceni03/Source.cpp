#include "Monstrum.h"
#include "StatickyObjekt.h"
#include "Hra.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

int main(int argc, char** argv) 
{
	Hra* game = new Hra();
	time_t startTime = time(nullptr);
	for (int i = 0; i < 1000; i++) {
		Objekt* obj;
		if (rand() % 2 == 0) {
			PohyblivyObjekt* pohObj = new PohyblivyObjekt(i,rand()%2*M_PI);
			pohObj->setX(rand() % 100 + 100);
			pohObj->setY(rand() % 100 + 100);
			obj = pohObj;
		}
		else {
			StatickyObjekt* statObj = new StatickyObjekt(i, TypPrekazky::VelkaRostlina);
			obj = statObj;
		}
		game->vlozObjekt(obj);
	}
	time_t endTime = time(nullptr);
	/*char* startBuffer = new char[20];
	ctime_s(startBuffer, 20, &startTime);
	char* endBuffer = new char[20];
	ctime_s(endBuffer, 20, &endTime);*/
	cout << "StartTime: " << startTime << ", EndTime: " << endTime<<endl;

	/*Objekt* obj1 = new StatickyObjekt(1, TypPrekazky::VelkaRostlina);
	obj1->setX(10);
	obj1->setY(55);
	StatickyObjekt* statObj = dynamic_cast<StatickyObjekt*>(obj1);
	if (statObj != nullptr) {
		cout << "obj1 je Staticky objekt (ci jeho potomek)" << endl;
		game->vlozObjekt(statObj);
	}
	
	Objekt* obj2 = new PohyblivyObjekt(2, M_PI);
	obj2->setX(15);
	obj2->setY(60);
	PohyblivyObjekt* pohObj = dynamic_cast<PohyblivyObjekt*>(obj2);
	if (pohObj != nullptr) {
		cout << "obj2 je Pohyblivy objekt (ci jeho potomek)" << endl;
		game->vlozObjekt(pohObj);
	}

	Objekt* obj3 = new Monstrum(3, 0.5*M_PI, 55, 100);
	obj3->setX(18);
	obj3->setY(57);
	Monstrum* monstrum = dynamic_cast<Monstrum*>(obj3);
	if (monstrum != nullptr) {
		cout << "obj3 je Monstrum" << endl;
		game->vlozObjekt(monstrum);
	}

	int* statObjekty = game->najdiIdStatickychObjektu(0, 1.5*M_PI, 50, 65);
	PohyblivyObjekt** pohObjekty = game->najdiPohybliveObjektyVOblasti(0, 1.5*M_PI, 10);*/
}