#ifndef MOSTRUM_H
#define MOSTRUM_H

#include "PohyblivyObjekt.h"
class Monstrum :
	public PohyblivyObjekt
{
public:
	~Monstrum();
	Monstrum(int aId, double aUhelNatoceni, int aHp, int aMaxHp);
	void setHp(int aHp);
	int getHp();
	void setMaxHp(int aMaxHp);
	int getMaxHp();
private:
	int hp;
	int maxHp;
};
#endif // !MOSTRUM_H

