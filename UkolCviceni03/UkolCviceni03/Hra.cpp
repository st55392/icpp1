#include "Hra.h"

Hra::Hra() {
	objekty = new Objekt*[];
}
Hra::~Hra() {
	delete[]objekty;
}

void Hra::vlozObjekt(Objekt* aObjekt)
{
	Objekt** objektyTemp = new Objekt*[pocetPrvku + 1];
	for (int i = 0; i < pocetPrvku; i++) {
		objektyTemp[i] = objekty[i];
	}
	objektyTemp[pocetPrvku++] = aObjekt;
	if (objekty != nullptr) 
		delete[]objekty;
	objekty = objektyTemp;
}

int* Hra::najdiIdStatickychObjektu(double xmin, double xmax, double ymin, double ymax) const
{
	int* idStatObj = nullptr;
	int pocetStatObj = 0;
	for (size_t i = 0; i < pocetPrvku; i++) { //nalezeni statickych objektu a zjisteni jejich poctu
		Objekt* obj = objekty[i];
		StatickyObjekt* statObj = dynamic_cast<StatickyObjekt*>(obj);
		if (statObj != nullptr) {
			if (jeObjektVOblasti(statObj,xmin,xmax,ymin,ymax)) {
				pocetStatObj++;
			}
		}
	}
	if (pocetStatObj == 0)
		return idStatObj;

	int indexPole = 0;
	idStatObj = new int[pocetStatObj];
	for (size_t i = 0; i < pocetPrvku; i++) {
		Objekt* obj = objekty[i];
		StatickyObjekt* statObj = dynamic_cast<StatickyObjekt*>(obj);
		if (statObj != nullptr) {
			if (jeObjektVOblasti(statObj, xmin, xmax, ymin, ymax)) {
				idStatObj[indexPole] = statObj->getId();
			}
		}
	}
	return idStatObj;
}

static bool jeObjektVOblasti(Objekt* aObjekt, double xmin, double xmax, double ymin, double ymax) {
	return aObjekt->getX() >= xmin && aObjekt->getY() >= ymin && aObjekt->getX() <= xmax && aObjekt->getY() <= ymax;
}

PohyblivyObjekt** Hra::najdiPohybliveObjektyVOblasti(double x, double y, double r)
{
	PohyblivyObjekt** hledaneObjekty = nullptr;
	int pocet = 0;
	for (int i = 0; i < pocetPrvku; i++) {
		Objekt* obj = objekty[i];
		PohyblivyObjekt* pohObj = dynamic_cast<PohyblivyObjekt*>(obj);
		//vyhledani pohyblivych objektu, ktere jsou v okruhu
		if (pohObj != nullptr) {
			if ((pow(pohObj->getX() - x, 2) + pow(pohObj->getY() - y, 2)) <= pow(r, 2)) {
				pocet++;
			}
		}
	}
	if (pocet == 0)
		return hledaneObjekty;
	hledaneObjekty = new PohyblivyObjekt * [pocet];
	int index = 0;
	for (int i = 0; i < pocetPrvku; i++) {
		Objekt* obj = objekty[i];
		PohyblivyObjekt* pohObj = dynamic_cast<PohyblivyObjekt*>(obj);
		//vyhledani pohyblivych objektu, ktere jsou v okruhu
		if (pohObj != nullptr) {
			if ((pow(pohObj->getX() - x, 2) + pow(pohObj->getY() - y, 2)) < pow(r, 2)) {
				hledaneObjekty[index] = pohObj;
				index++;
			}
		}
	}
	return hledaneObjekty;
}

PohyblivyObjekt** Hra::najdiPohybliveObjektyVOblasti(double x, double y, double r, double umin, double umax)
{
	PohyblivyObjekt** hledaneObjekty;
	size_t pocet = 0;
	//zjisteni poctu hledanych prvku
	for (size_t i = 0; i < pocetPrvku; i++) { 
		Objekt* obj = objekty[i];
		PohyblivyObjekt* pohObj = dynamic_cast<PohyblivyObjekt*>(obj);
		if (pohObj != nullptr) {
			if ((pow(pohObj->getX() - x, 2) + pow(pohObj->getY() - y, 2)) < pow(r, 2)) {
				if (pohObj->getUhelNatoceni() > umin && pohObj->getUhelNatoceni() < umax) {
					pocet++;
				}
			}
		}
	}
	hledaneObjekty = new PohyblivyObjekt * [pocet]; //alokace pole
	size_t index = 0;
	for (size_t i = 0; i < pocetPrvku; i++) {
		Objekt* obj = objekty[i];
		PohyblivyObjekt* pohObj = dynamic_cast<PohyblivyObjekt*>(obj);
		if (pohObj != nullptr) {
			if ((pow(pohObj->getX() - x, 2) + pow(pohObj->getY() - y, 2)) < pow(r, 2)) {
				if (pohObj->getUhelNatoceni() > umin && pohObj->getUhelNatoceni() < umax) {
					hledaneObjekty[index] = pohObj;
					index++;
				}
			}
		}
	}
	return hledaneObjekty;
}

