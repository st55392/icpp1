#ifndef POHYBLIVY_OBJEKT_H
#define POHYBLIVY_OBJEKT_H
#include "Objekt.h"
#define _USE_MATH_DEFINES
#include<math.h>
#include <stdexcept> 
class PohyblivyObjekt : public Objekt
{
public:
	~PohyblivyObjekt();
	PohyblivyObjekt(int aId, double aUhelNatoceni);
	void setUhelNatoceni(double aUhelNatoceni);
	double getUhelNatoceni();
private:
	double uhelNatoceni=0;
};
#endif // !POHYBLIVY_OBJEKT_H

