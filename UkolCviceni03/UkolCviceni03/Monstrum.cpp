#include "Monstrum.h"
Monstrum::~Monstrum()
{
}
Monstrum::Monstrum(int aId, double aUhelNatoceni, int aHp, int aMaxHp) :hp(aHp), maxHp(aMaxHp), PohyblivyObjekt(aId, aUhelNatoceni) {

};

int Monstrum::getHp() {
	return hp;
}

void Monstrum::setHp(int aHp) {
	hp = aHp;
}

int Monstrum::getMaxHp() {
	return maxHp;
}

void Monstrum::setMaxHp(int aMaxHp) {
	maxHp = aMaxHp;
}
