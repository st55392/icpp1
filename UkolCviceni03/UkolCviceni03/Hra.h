#ifndef HRA_H
#define HRA_H
#include "Monstrum.h"
#include "StatickyObjekt.h"

class Hra
{
public:
	Hra();
	~Hra();
	void vlozObjekt(Objekt* aObjekt);
	int* najdiIdStatickychObjektu(double xmin, double xmax, double ymin, double ymax)const;
	PohyblivyObjekt** najdiPohybliveObjektyVOblasti(double x, double y, double r);
	PohyblivyObjekt** najdiPohybliveObjektyVOblasti(double x, double y, double r, double umin, double umax);
	int pocetPrvku = 0;
private:
	Objekt** objekty;
};
#endif // !HRA_H

