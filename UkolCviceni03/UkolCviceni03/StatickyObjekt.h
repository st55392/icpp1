#ifndef STATICKY_OBJEKT_H
#define STATICKY_OBJEKT_H
#include "Objekt.h"
enum TypPrekazky { Skala, MalaRostlina, VelkaRostlina };
class StatickyObjekt :public Objekt
{
public:
	StatickyObjekt(int aId, TypPrekazky aTypPrekazky);
	~StatickyObjekt();
	TypPrekazky getTypPrekazky();
private:
	TypPrekazky typPrekazky;
};

#endif // STATICKY_OBJEKT_H

