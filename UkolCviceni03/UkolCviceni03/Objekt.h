#ifndef OBJEKT_H
#define OBJEKT_H

class Objekt{
public:
	Objekt(int aId);
	virtual ~Objekt();
	void setX(double aX);
	void setY(double aY);
	double getX();
	double getY();
	int getId();
private:
	int id;
	double x;
	double y;
};

#endif // OBJEKT_H



