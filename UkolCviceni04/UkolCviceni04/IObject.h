#ifndef I_OBJECT_H
#define I_OBJECT_H
#include<iostream>
#include<string>

class IObject{
public:
	IObject() {};
	virtual ~IObject() {};
	virtual std::string toString() const=0; //ciste virtualni metoda =0
};

#endif // !I_OBJECT_H
