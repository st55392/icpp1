#include "IObject.h"
#include "Time.h"
#include<algorithm>

void SeraditPole(IComparable** objects, int numberOfObjects) {
	IComparable* actHighest;
	int indexActHighest;
	IComparable* temp;
	for (size_t i = 0; i < numberOfObjects; i++) {
		actHighest = objects[i];
		indexActHighest = i;
		for (size_t j = i + 1; j < numberOfObjects; j++) {
			if (actHighest->compareTo(objects[j]) == -1) {
				actHighest = objects[j];
				indexActHighest = j;
			}
		}
		temp = objects[i];
		objects[i] = actHighest;
		objects[indexActHighest] = temp;
	}
}

int main(int argc, char** argv) {
	IComparable** objects = new IComparable*[100];
	int numberOfObjects=0;

	for (size_t i = 0; i < 100; i++) {
		int hours = rand() % 24;
		int minutes = rand() % 60;
		int seconds = rand() % 60;
		Time* time = new Time(hours, minutes, seconds);
		objects[i] = time;
		numberOfObjects++;
	}
	SeraditPole(objects, numberOfObjects);
	for (size_t i = 0; i < numberOfObjects; i++) {
		cout << objects[i]->toString() << endl;
	}
	for (size_t i = 0; i < numberOfObjects; i++) {
		delete objects[i];
	}
	delete[]objects;

}