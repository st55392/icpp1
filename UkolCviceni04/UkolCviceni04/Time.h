#ifndef TIME_H
#define TIME_H
#include<string>
#include"IComparable.h"
using namespace std;

struct Time :IComparable {
public:
	Time(int aHours, int aMinutes, int aSeconds);
	~Time() {};
	virtual int compareTo(IComparable* obj) const override; 
	//override	zajistuje kontrolu, zda p�epis odpov�d� virtualni metode predka
	virtual string toString() const override;
private:
	int hours;
	int minutes;
	int seconds;
};
#endif // !TIME_H