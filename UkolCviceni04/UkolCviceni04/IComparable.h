#ifndef I_COMPARABLE_H
#define I_COMPARABLE_H
#include "IObject.h"

class IComparable :public IObject {
public:
	IComparable() {};
	virtual ~IComparable() {}; //virtualni destruktor
	virtual int compareTo(IComparable* obj) const=0;
};
#endif // !I_COMPARABLE_H