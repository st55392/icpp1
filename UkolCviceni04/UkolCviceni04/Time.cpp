#include "Time.h"
#include <stdexcept>


Time::Time(int aHours, int aMinutes, int aSeconds) {
	if (aHours >= 0 && aHours < 24 && aMinutes >= 0 && aMinutes <= 60 && aSeconds >= 0 && aSeconds <= 60) {
		hours = aHours;
		minutes = aMinutes;
		seconds = aSeconds;
	}
	else {
		throw std::invalid_argument("Hours must be 0-23, minutes 0-60, seconds 0-60");
	}
}

int Time::compareTo(IComparable* obj) const {
	Time* time = dynamic_cast<Time*>(obj);
	if (time != nullptr) {
		if (this->hours > time->hours) {
			return 1;
		}
		else if (this->hours == time->hours) {
			if (this->minutes > time->minutes) {
				return 1;
			}
			else if (this->minutes == time->minutes) {
				if (this->seconds > time->seconds) {
					return 1;
				}
				else if (this->seconds == time->seconds) {
					return 0;
				}
				else {
					return -1;
				}
			}
			else {
				return -1;
			}
		}
		else {
			return -1;
		}
	}else {
		throw std::invalid_argument("Object is not TIME");
	}
}

string Time::toString() const {
	string hours;
	string minutes;
	string seconds;
	if (this->hours < 10) {
		 hours = "0" + to_string(this->hours);
	}
	else {
		 hours = to_string(this->hours);
	}if (this->minutes < 10) {
		minutes = "0" + to_string(this->minutes);
	}
	else {
		minutes = to_string(this->minutes);
	}if (this->seconds < 10) {
		seconds = "0" + to_string(this->seconds);
	}
	else {
		seconds = to_string(this->seconds);
	}
	return hours + ":" + minutes+ ":" +seconds;
}