#include"potrubi.h"
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

IPotrubi* nactiPotrubi(string cestaKSouboru)
{
	IPotrubi* potrubi;
	int velikost = 0;
	ifstream in;
	string radek;
	char znak;
	in.open(cestaKSouboru);
	if (in.is_open()) {
		getline(in, radek);
		velikost = stoi(radek);
		potrubi = new Potrubi(velikost);
		int i = 0;
		while (getline(in, radek)) {
			for (int j = 0; j < radek.length(); j++) {
				znak = radek.at(j);
				switch (znak) {
				case '-':
					potrubi->VlozPrvek(new VychodZapad(i, j));
					break;
				case '+':
					potrubi->VlozPrvek(new SeverJihZapadVychod(i, j));
					break;
				case 'T':
					potrubi->VlozPrvek(new JihVychodZapad(i, j));
					break;
				case 'I':
					potrubi->VlozPrvek(new SeverJih(i, j));
				case 'O':
					potrubi->VlozPrvek(new Nic(i, j));
					break;
				}
			}
			i++;
		}
		in.close();
		return potrubi;
	}
	return nullptr;
}

int main(int argc, char** argv) {
	IPotrubi* potrubi1 = nactiPotrubi("test1.txt");
	if (potrubi1->JePotrubiOk()){
		std::cout<<"potrubi1 je ok"<<std::endl;
	}
	else {
		std::cout << "potrubi1 neni ok" << std::endl;
	}

	IPotrubi* potrubi2 = nactiPotrubi("test2.txt");
	if (potrubi2->JePotrubiOk()) {
		std::cout << "potrubi2 je ok" << std::endl;
	}
	else {
		std::cout << "potrubi2 neni ok" << std::endl;
	}

	IPotrubi* potrubi3 = nactiPotrubi("test3.txt");
	if (potrubi3->JePotrubiOk()) {
		std::cout << "potrubi3 je ok" << std::endl;
	}
	else {
		std::cout << "potrubi3 neni ok" << std::endl;
	}

	IPotrubi* potrubi4 = nactiPotrubi("test4.txt");
	if (potrubi4->JePotrubiOk()) {
		std::cout << "potrubi4 je ok" << std::endl;
	}
	else {
		std::cout << "potrubi4 neni ok" << std::endl;
	}

	IPotrubi* potrubi5 = nactiPotrubi("test5.txt");
	if (potrubi5->JePotrubiOk()) {
		std::cout << "potrubi5 je ok" << std::endl;
	}
	else {
		std::cout << "potrubi5 neni ok" << std::endl;
	}

	IPotrubi* potrubi6 = nactiPotrubi("test6.txt");
	if (potrubi6->JePotrubiOk()) {
		std::cout << "potrubi6 je ok" << std::endl;
	}
	else {
		std::cout << "potrubi6 neni ok" << std::endl;
	}

	IPotrubi* potrubi7 = nactiPotrubi("test7.txt");
	if (potrubi7->JePotrubiOk()) {
		std::cout << "potrubi7 je ok" << std::endl;
	}
	else {
		std::cout << "potrubi7 neni ok" << std::endl;
	}

	IPotrubi* potrubi8 = nactiPotrubi("test8.txt");
	if (potrubi8->JePotrubiOk()) {
		std::cout << "potrubi8 je ok" << std::endl;
	}
	else {
		std::cout << "potrubi8 neni ok" << std::endl;
	}

	return 0;
}
