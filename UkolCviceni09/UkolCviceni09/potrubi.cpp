#include "potrubi.h"
#include "fstream"
using namespace std;
Potrubi::Potrubi(int aVelikost):velikost(aVelikost)
{
	prvky = new APotrubniPrvek** [velikost];
	for (int i = 0; i < velikost; i++) {
		prvky[i] = new APotrubniPrvek* [velikost];
	}
	for (int i = 0; i < velikost; i++) {
		for (int j = 0; j < velikost; j++) {
			prvky[i][j] = nullptr;
		}
	}
}

Potrubi::~Potrubi()
{
	for (int i = 0; i < velikost; i++) {
		delete [] prvky[i];
	}
	delete prvky;
}

void Potrubi::VlozPrvek(APotrubniPrvek* aPrvek)
{
	int x = aPrvek->_x;
	int y = aPrvek->_y;
	prvky[x][y] = aPrvek;
}

const APotrubniPrvek* Potrubi::DejPrvek(int aX, int aY) const
{
	if (aX < 0 || aX > velikost - 1 || aY<0 || aY>velikost - 1)
		return nullptr;

	return prvky[aX][aY];
}

bool Potrubi::JePotrubiOk() const
{
	for (int i = 0; i < velikost; i++) {
		for (int j = 0; j < velikost; j++) {
			if (prvky[i][j] != nullptr) {
				if (!(prvky[i][j]->JeKorektneZapojeno(this))) {
					return false;
				}
			}
		}
	}
}



VychodZapad::VychodZapad(int aX, int aY)
{
	this->_x = aX;
	this->_y = aY;
}

bool VychodZapad::JeKorektneZapojeno(const IPotrubi* potrubi)const {
	if (potrubi->DejPrvek(_x, _y - 1) == nullptr || potrubi->DejPrvek(_x, _y + 1) == nullptr) {
		return false;
	}
	return true;
}

SeverJih::SeverJih(int aX, int aY)
{
	this->_x = aX;
	this->_y = aY;
}

bool SeverJih::JeKorektneZapojeno(const IPotrubi* potrubi) const
{
	if (potrubi->DejPrvek(_x-1, _y) == nullptr || potrubi->DejPrvek(_x+1, _y) == nullptr) {
		return false;
	}
	return true;
}

SeverJihZapadVychod::SeverJihZapadVychod(int aX, int aY)
{
	this->_x = aX;
	this->_y = aY;
}

bool SeverJihZapadVychod::JeKorektneZapojeno(const IPotrubi* potrubi) const
{
	if (potrubi->DejPrvek(_x, _y - 1) == nullptr || potrubi->DejPrvek(_x, _y + 1) == nullptr
		|| potrubi->DejPrvek(_x - 1, _y) == nullptr || potrubi->DejPrvek(_x + 1, _y) == nullptr) {
		return false;
	}
	return true;
}

Nic::Nic(int aX, int aY)
{
	this->_x = aX;
	this->_y = aY;
}

bool Nic::JeKorektneZapojeno(const IPotrubi* potrubi) const
{
	return true;
}

JihVychodZapad::JihVychodZapad(int aX, int aY)
{
	this->_x = aX;
	this->_y = aY;
}

bool JihVychodZapad::JeKorektneZapojeno(const IPotrubi* potrubi) const
{
	if (potrubi->DejPrvek(_x, _y + 1) == nullptr
		|| potrubi->DejPrvek(_x, _y-1) == nullptr || potrubi->DejPrvek(_x + 1, _y) == nullptr) {
		return false;
	}
	return true;
}
