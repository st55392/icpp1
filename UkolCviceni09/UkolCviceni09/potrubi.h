#ifndef POTRUBI_H
#define POTRUBI_H
#include <iostream>
#include <fstream>
struct APotrubniPrvek;
struct IPotrubi {
	virtual ~IPotrubi() {};
	virtual const APotrubniPrvek* DejPrvek(int aX, int aY)const = 0;
	virtual bool JePotrubiOk()const = 0;
	virtual void VlozPrvek(APotrubniPrvek* aPrvek)= 0;
};


struct Potrubi : IPotrubi {
private:
	int velikost;
	APotrubniPrvek*** prvky;
public:
	Potrubi(int aVelikost);
	~Potrubi();
	virtual void VlozPrvek(APotrubniPrvek* aPrvek);
	virtual const APotrubniPrvek* DejPrvek(int aX, int aY)const;
	virtual bool JePotrubiOk()const;
};

struct APotrubniPrvek {
	virtual ~APotrubniPrvek() { };
	virtual bool JeKorektneZapojeno(const IPotrubi* potrubi)const = 0;
	int _x;
	int _y;
};

struct VychodZapad : APotrubniPrvek {
	VychodZapad(int aX, int aY);
	~VychodZapad() {};
	bool JeKorektneZapojeno(const IPotrubi* potrubi)const override;
};

struct SeverJih : APotrubniPrvek {
	SeverJih(int aX, int aY);
	~SeverJih() {};
	bool JeKorektneZapojeno(const IPotrubi* potrubi)const override;
};

struct SeverJihZapadVychod : APotrubniPrvek {
	SeverJihZapadVychod(int aX, int aY);
	~SeverJihZapadVychod() {};
	bool JeKorektneZapojeno(const IPotrubi* potrubi)const override;
};

struct Nic : APotrubniPrvek {
	Nic(int aX, int aY);
	~Nic() {};
	bool JeKorektneZapojeno(const IPotrubi* potrubi)const override;
};

struct JihVychodZapad : APotrubniPrvek {
	JihVychodZapad(int aX, int aY);
	~JihVychodZapad() {};
	bool JeKorektneZapojeno(const IPotrubi* potrubi)const override;
};

#endif // !POTRUBI_H
