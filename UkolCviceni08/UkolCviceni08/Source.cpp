#include "Matice.h"
#include<iostream>

int main()
{
	Matice<int> m{ 3,3 };
	int jednodpole[] = { 0,1,2,3,4,5,6,7,8 };
	m.NastavZ(jednodpole,9);
	std::cout << "Matice m: " << std::endl;
	m.Vypis();

	Matice<int> mt = m.Transpozice();
	std::cout << "Matice m transponovana: " << std::endl;
	mt.Vypis();

	Matice<int> mmt = m.Soucin(mt);
	std::cout << "Matice m*transponovana: " << std::endl;
	mmt.Vypis();

	Matice<double> mmt_d = mmt.Pretypuj<double>();
	std::cout << "Matice mmt pretypovana: " << std::endl;
	mmt_d.Vypis();

	Matice<double> n_d{ 3,3 };
	double jednodpole_d[] = { 4.5,5,0,2,-0.5,7,1.5,9,6 };
	n_d.NastavZ(jednodpole_d,9);
	std::cout << "Matice double z pole: " << std::endl;
	n_d.Vypis();

	Matice<double> mmtn_d = mmt_d.Soucin(n_d);
	std::cout << "Matice mmt_d*n_d: " << std::endl;
	mmtn_d.Vypis();

	Matice<int> r = mmtn_d.Pretypuj<int>();
	Matice<int> t{ 3,3 };
	int tpole[] = { 85,225,236,292,819,866,499,1413,1496 };
	t.NastavZ(tpole,9);
	std::cout << "r==t ? " << (r.JeShodna(t) ? "true" : "false") << std::endl;
	return 0;
}